---
layout: page
published: true
title: Projects
date: 2014-01-16 19:31:57 +0000
categories: SitePage
tag: projects
---

Earlier this year, I've concentrated on system security.  This involved breaking out a new laptop out-of-the-box, and re-formatting the hard drive.  From there, a methodical installation of both Windows and Ubuntu on separate partitions was accompanied into research into Truecrypt Full-Disk-Encryption (FDE) along with other defense measures.  Some of these measures include Reboot-To-Restore software, which sites "outside" the operating system, and re-images the hard drive with every reboot; this ensures that most malware would ever survive a reboot, if ever infected. 
In all, here's a rough summary of most things I've been exploring:

- Windows OS
- Ubuntu OS
- KeyScrambler
- Bleachbit
- DriveVaccine
- Truecrypt
- Thunderbird e-mail client (portable)
- PGP
- Pidgin chat client (portable)
- OTR key generation (secure chatting)
- Other system customization efforts


More recently, I've taken an interest in the following open source projects:

- OpenVPN
- Owncloud
- FreeNAS
- Ubuntu Server

If you have any comments, suggestions, or questions about these efforts, by all means, send me a message!




[jekyll-gh]: https://github.com/mojombo/jekyll
[jekyll]:    http://jekyllrb.com
