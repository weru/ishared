(function() {

  let $nav = $('.nav');

  $('.nav__toggle').on('click', () => {
    $nav.toggleClass('close__menu').toggleClass('open__menu');
  });

  function close__menu() {
    $nav.toggleClass('open__menu');
    let $opened_before = $nav.hasClass('close__menu');
    $opened_before ? $nav.toggleClass('close__menu') : false;
  }

  $close_buttons = ['.toggle', '.overlay'];

  $close_buttons.map( (button) => {
    $(button).on('click', () => {
      close__menu();
    });
  });

})();

(function year() {
  let $date = new Date();
  let $year = $date.getFullYear();
  $('.year').text($year);
})();

(function copy() {
  $clipboard = $('pre.highlight');

  $clipboard.before(`<div class = 'highlight__copy'>Copy</div>`);
  $button = $('.highlight__copy');
  $button.on('click', function(e) {

    let $sibling = $(this).siblings();
    let $child = $sibling.children()[0];
    let $temp = $(`<div class = 'hidden'>`);

    $clipboard.before($temp);

    $temp.attr("contenteditable", true)
        .html($child.innerText).select()
        .on("focus", function() { document.execCommand('selectAll',false,null) })
        .focus();
    document.execCommand("copy");
    $button.text('Copied')
    $temp.remove();

    function reset_button(){
      $button.html('Copy');
    }

    setTimeout(reset_button, 3000);

  });

})();

