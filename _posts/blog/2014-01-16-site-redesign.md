---
published: true
title: Site Redesign
date: 2014-01-16 19:31:57 +0000
categories: blog
tags: SiteNews
permalink: "/blog/2014/01/16/site-redesign/"
threatmodel: 'Threat Model: Information Gathering Organizations'
---

{{site.tags.sitenews}}


After some careful consideration, I made the decision to forego Wordpress and instead opt for a static web site. 

Why?
Other popular alternatives--while easy to install, design, and maintain--are typically dependent on password-protected databases, plugins, and web software; each of these are inherently vulnerable to hacking.

How is Jekyll more secure?
Jekyll is a software suite written in Ruby which runs directly on an author's home computer. The author is free to design, write, preview, and generate the finished product from home. Once generated, all the user has to do is copy all generated files (which are kept in a single folder) onto the web server.  So, unlike other blogging engines, Jekyll does not require sensitive password information, or web programming code to be hosted onto the web server.  

What about site quality?
Well, what this means is that all design and maintenance is done manually.  While time consuming, taking this route reflects my decision to lend security a much higher priority over aesthetics.

Which, I hope you'll agree is a choice most appropriate for a site which is all cyber security.  





[jekyll-gh]: https://github.com/mojombo/jekyll
[jekyll]:    http://jekyllrb.com
