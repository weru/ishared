---
Title: On Rootkits
categories: DraftIdeas Malware
Tags: drafts
Threat Model: Skilled Attackers, Information Gathering Organizations
date: 2014-01-24 00:00:00 +0000
---

There is probably no better way to learn about the "Hows" and "Whys" behind system hardening than to do a study on Rootkits.  These are particularly vicious pieces of malware that are designed for stealth, such that conventional malware detection methods often fail.  

With the latest revelations concerning current (even if secret) real-world surveillance capabilities, I've become especially motivated to gain an enhanced understanding about the nature of the threat and the best way to protect against it. 

There are, by some counts, at least four or five levels of severity with rootkit attacks.  Two which caught most of my attention were Master Boot Record (MBR) and BIOS/Firmware infections.  With the former, the only means of defeating these rootkits is to rely on tools which live and operate outside the Operating System--that is, before you even see a Windows logo.

A few methods are routinely offered, among them:

- Scanning hard drive as an external storage device with a different machine. This method tends to be more effective, because while in an offline state, MBR Rootkits are incapable of doing the work of self-concealment and are easier to spot.

- Re-imaging or re-formatting the hard drive.  I'd include disk repartitioning in this category.

- Restore-on-Reboot technology.  This involves software which lives "outside" of the operating system for the sole purpose of reflashing the Operating system partition to a previous clean state upon every reboot.  The software takes a snapshot of a machine upon installation, and reverts the entire OS partition back to the exact state the baseline snapshot took place, which effectively nullifies any changes made to the system (including software addition/removals, system configuration settings, file deletions, and infections).  

My preference is for the third option, as it is by far the least cumbersome, and effectively re-images the entire system partition **on every reboot**.  It's automated consistency, with a focus on keeping systems pristine and secure. Some Windows-based solutions I'm looking at include Faronics Deep Freeze, Disk Shadow, Restore Reboot, and Drive Vaccine--the latter two of which come from the same software development company. 

I'll review and explain them in more detail in a different post.  But there's a final, and most difficult to treat type of rootkit, and that's BIOS (or firmware)-based.  

Several control measures, from most intrusive to the least:

- Hardware disabling of flashing the BIOS.  A recent (2013) 30th Chaos Conference presentation featured a well-versed professional in hardware security.  He demonstrates how he managed to identify where on the circuitboard chip one would need to do some solder work to physically disable the BIOS chip from being flashed--making this a pretty effective form of write-protection.

- Reflashing the BIOS.  Most manufacturers offer BIOS updates.  It's good practice to always keep all basic drivers and documentation in offline backup storage just in case.  However, if BIOS could stand to be re-flashed on every reboot, this would be especially secure. 

- TPM:  There are various integrity checks that are offered natively as part of the system architecture.  Each system and implementation is unique, so it'd be impossible to give an adequate treatment of how this option works in every case.  

My inability to elaborate on that last countermeasure raises another interesting observation for the security-conscious:  As devastating an attack as this is, the real-world threat it poses against your machine may well be miniscule.  I'll close with one very interesting observation about the actual likelihood of this threat, which actually... makes a *lot* of sense:

>It's not that [malicious coders] don't know how to do it [write more BIOS rootkits], it's just that as an infection vector, it just doesn't make much sense.

> BIOS firmware is very specific to a given machine's motherboard/chipset.  This means a lot of time is going to be spent by the malware author writing very specific code that is only going to affect a very small subset of machines out there.  The payoff just isn't there.

> The only way this is even slightly viable, is if the malware author knows where there is a large number of machines with identical hardware. (Like a corporate environment).

> And even then, flashing the BIOS isn't an invisible operation.  Chances are really good that the user is going to see this occuring and know it isn't normal operation of their machine, and will likely turn it off during the procudure.  (most likely bricking the machine)

> This is why most instances of BIOS malware are merely proof-of-concept.  BIOS malware continues to be extremely rare in the wild, even though as you've pointed out, the concept has been around for years.

 
