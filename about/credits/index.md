---
layout: page
published: true
title: Thanks
permalink: "/about/thanks/"
categories: SitePage
tag: credits
---
The following resources were instrumental in building this site from virtual scratch, and I extend my gratitude for their considerable talent and generosity in sharing their knowledge with others:

- <http://css3gen.com/box-shadow/>
- <http://css3generator.com/>
- <http://www.colorzilla.com/gradient-editor/>
- <http://colorschemedesigner.com/#4921Tw0w0w0w0>
- <http://matthewjamestaylor.com/blog/keeping-footers-at-the-bottom-of-the-page>
- <http://www.patternify.com/>
- <http://www.google.com/fonts>
- <http://csslint.net/>
- <https://github.com/scottwb/jekyll-tweet-tag>
- <http://stackoverflow.com/questions/806000/css-semi-transparent-background-but-not-text>
- <http://stackoverflow.com/questions/9794699/listing-all-the-blog-posts-with-content-with-jekyll>
- <http://stackedit.io> and <http://dillinger.io>

### For possible implementation

- <http://twitcker.com/example/>
- <http://www.menucool.com/tooltip/css-tooltip
- <http://www.webdesignerdepot.com/2012/11/how-to-create-a-simple-css3-tooltip/>


### Risk

This section lists all known risks user assumes by use of the site which are above and beyond risks normally associated with browsing static html pages.

### Javascript

Comments.   The comments feature is run by Disqus, and its implementation requires the use of javascript.  at present, I am unaware of what specific risks a user faces by enabling this function.