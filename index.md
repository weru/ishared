---
layout: page
published: true
title: Home
date: 2014-01-16 19:31:57 +0000
categories: SitePage
tags: Home
---
### Welcome to SharedIntellect

SharedIntellect was launched in January 2014, out of a commitment to make personal security technologies more accessible to people. 

While larger organizations have entire teams at focused on protecting *their* own assets and information, this often leaves consumers, activists, and individuals without the resources or the expertise to defend against eavesdropping, data compromise, and damaged reputations. 

I founded this site to offer consultation, training workshops, and speaking engagements to individuals with an interest in securing their information, communication, and reputation in an increasingly hostile world.  

Right now, I'm available for any inquiries, though in the future special attention will be given to:

- Non-profit charity leaders and their teams
- Community organizations
- Multi-campus student organizations
- Campaigns and activists
- Conferences and Conventions

### Youtube Video

{% youtube kWmX3pd1f10 %}

### Vimeo Video

{% vimeo 214005594 %}